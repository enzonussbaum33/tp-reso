 # Tp partie 1 Premier pas réseau 
 
 ## Affichez les infos des cartes réseau de votre PC
 
 ipconfig /all 
 vas nous permettre d avoir toute les info sur le reseau 
 
  Carte réseau sans fil Wi-Fi 
  30-03-C8-E7-7C-25 adresse mac 
  10.33.16.94 adresse ipv4
 
  j ai pas de port ethernet donc pas de reseau Ethernet 

 ### afficher votre gateway
 
  ipconfig /all 
 vas nous permettre d avoir toute les info sur le reseau 

  10.33.19.254 

 ### Déterminer la MAC de la passerelle
  arp -a 
  permet de voir les passerelle + leur adresse MAC 
  
  00-c0-e7-e0-04-4e

 # Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
 
- panneau de config 
- afficher l etat et la gestion du reseau 
- Wi-Fi (WiFi@YNOV)
- detail 
 10.33.16.194
 - adresse ip
 30-03-C8-E7-7C-258
 - adresse MAC
 10.33.16.254
 - gateway
 
 # Utilisez l'interface graphique de votre OS pour changer d'adresse IP : 
 
- panneau de config 
- afficher l etat et la gestion du reseau 
- Wi-Fi (WiFi@YNOV)
- Propriétés
- double clique Protocole Internet Version 4 (TCP/IPv4)
- utiliser l adresse suivant : changer l adresse IP
- ok puis encore ok

 ### Il est possible que vous perdiez l'accès internet.
 
 en changeant l adresse IP manuellement il est possible de ce retrouver sur la meme adresse IP q une autre        personne pour envoyer du pc ca vait mais des que nous devons recevoir un paquet il ne sauras pas ou allez nous n avons donc pas internet il faut donc refaire les meme étape mes mettre obtenir une adresse IP aleatoirement 
 
### Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

- connecter les deux pc avec un cable RJ45 
- on modifie les adresse IP 
- se connecter au meme reseaux

### Vérifier à l'aide d'une commande que votre IP a bien été changée
- IPconfig 

### Vérifier que les deux machines se joignent

- PING l adresse IP de l autre PC 

### Déterminer l'adresse MAC de votre correspondant

- ARP -a 
- trouver son adresse IP puis notée son adresse MAC 

# Tester l'accès internet
coté hebergereur 
- panneau de configuration
- afficher l etat et la gestion du reseau
- modifier les paramettre de la carte 
- clique droit puis proprieter 
- partage puis cocher la premiere case 
- puis appuyer sur ok 
coté recepteur 
- ce mettre sur une meme adresse IP 
Ex: 192.168.137.1 pour le host ET 192.168.137.2 pour le receveur 
- il ping tout les deux leur adresse IP pour voir si il sont bien connecter 
pour se connecter a un serveur internette il faut un DNS si elle est pas activer allez sur le 
- panneau de config 
- afficher l etat et la gestion du reseau 
- Wi-Fi (WiFi@YNOV)
- Propriétés
- double clique Protocole Internet Version 4 (TCP/IPv4)
- puis mettez adresse DNS Automatiquement 

### Prouver que la connexion Internet passe bien par l'autre PC
coté receveur 
- traceroute 1.1.1.1
il vas afficher un gateway il faut verifier que c est le bon gateway, celui du HOST

# Petit chat privé

- sur le pc serveur installer NetCat
- puis ecrire la commande nc.exe -1 -p 8888
( pour demander au pc d ecouter sur le port 8888 )

- sur le pc client insatller NetCat 
- ecrire la commande nc.exe "l ip du pc serveur"puis le port sur le quelle le pc serveur ecoute ici 8888
ici les deux pc peuvent ecrire pour communiquer entre eux 

### visualiser la connexion en cours

- effectuer la commande netstat -a -n -b dans un powershell administrateur 
- chercher son adresse IP ici ca ce fini par 8888 et verifier quil y a bien marquer establish 

### Pour aller un peu plus loin
- nc.exe -1 -p "le numero de port" -s "IP_ADDRESS"
le serveur vas dire que toute les personne ayant la connaisance du port ici 8888 peut si connecter que si ils ont l IP

- nc.exe -1 -p "le numero de port"
alors qu en effectuer la commande sans le -s tout le monde peut si connecter grace au wifi apres en verifient 
nous verrons que notre adresse IP est a 0.0.0.8888

# 6. Firewall

## Activez et configurez votre firewall

### autoriser les ping
- Appuyez sur Démarrer, tapez "pare-feu Windows avec", puis lancez "Pare-feu Windows avec sécurité avancée".
- Vous allez créer une nouvelle règles: une pour autoriser les requêtes ICMPv4
- Vous allez créer deux nouvelles règles: une pour autoriser les requêtes ICMPv4
- Dans la fenêtre "Assistant Nouvelle règle entrante" , sélectionnez  "Personnalisé", puis cliquez sur "Suivant"
- Sur la page suivante, assurez-vous que "Tous les programmes" est sélectionné, puis cliquez sur "Suivant".
- Sur la page suivante, choisissez "ICMPv4" dans la liste déroulante "Type de protocole", puis cliquez sur le bouton "Personnaliser".
- Dans la fenêtre "Personnaliser les paramètres ICMP", sélectionnez l’option "Types ICMP spécifiques". Dans la liste des types ICMP, activez "Demande d’écho", puis cliquez sur "OK".
- De retour dans la fenêtre "Assistant Nouvelle règle entrante", vous êtes prêt à cliquer sur "Suivant".
- Sur la page suivante, il est plus simple de s’assurer que les options "Toute adresse IP" sont sélectionnées pour les adresses IP locales et distantes puis faite suivant
- Sur la page suivante, assurez-vous que l’option         "Autoriser la connexion" est activée, puis cliquez sur   "Suivant".
- La page suivante vous permet de contrôler le moment où la règle est active- Tous sélectionner et suivant
- Et enfin donner le nom que l'on souhaite donc ping ICMP

### autoriser le traffic sur le port qu'utilise nc

- Retourner au même endroit pour créer une nouvelle règle dans le trafic entrant
- Dans la page Type de règle de l’Assistant Nouvelle règle de trafic entrant, cliquez sur Personnalisé, puis sur Suivant.
- Dans la page Programme , cliquez sur Tous les programmes, puis sur Suivant
- Dans la page Protocole et ports , sélectionnez le type de protocole que vous souhaitez autoriser. Pour limiter la règle à un numéro de port spécifié, vous devez sélectionner TCP ou UDP. Dans notre cas nous pouvons spécifié notre port et alors une echelle de port.
- Une fois que vous avez configuré les protocoles et les ports, cliquez sur Suivant.
- Dans la page Étendue cliquez sur Suivant
- Dans la page Action , sélectionnez Autoriser la connexion, puis cliquez sur Suivant.
- Dans la page Profil , sélectionnez les types d’emplacement réseau auxquels cette règle s’applique, puis cliquez sur Suivant. Pour nous on sélectionne tous.
- Dans la page Nom , tapez un nom et une description pour votre règle, puis cliquez sur Terminer, Dans notre cas je l'ai appelé TCP.

# Exploration du DHCP, depuis votre PC
- ouvvrir un powershell est effectuerla commande   ipconfig /all
- chercher le serveur dhcp ici  192.168.60.254

- le bail expirat a l heure ou je le fait le jeudi 6 octobre 2022 11:04:19


# DNS

### Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

- ce sont toute les IP DNS utilisable par le pc si la premiere marche pas utiliser la deuxieme 
 8.8.8.8
 8.8.4.4
 1.1.1.1
 
 ### Utiliser, en ligne de commande l'outil nslookup pour faire des requêtes DNS à la main
 
 - pour google.com on tombe sur 142.250.179.110
 - et pour le site ynov.com on tombe sur
 172.67.74.226
 104.26.11.233
 104.26.10.233 
 
 ### reverse lookup 
 
 - la commande reverse lookup n est pas vraiment une commande nous allons plutot faire un lookup sur l adresse directement donc ici "ns lookup 231.34.113.12" il nous reveras le nom de domaine ici dns.google et la deuxieme 78.34.2.17 me donne aussi dns.google mais il me donne aussi un nom de domaine "cable-78-34-2-17.nc.de"

# Wireshark

voici a quoi resamble un ping un NetCat est un DNS depuis wireshark 

- un ping
![](https://i.imgur.com/hYpm8Y6.png)

- un NetCat 
![](https://i.imgur.com/g7y391B.png)

- un DNS
![](https://i.imgur.com/4KNwihF.png)