# Tp 2 Ethernet, IP, et ARP

##  Mettez en place une configuration réseau fonctionnelle entre les deux machines
- lancer un powershell en administrateur 
- effectuer la commande netsh 
cela permet de faire des modification depuis un invitée 

- puis effectuer interface ipv4 
pour lui dire que l on vas modifier l IPv4

- puis effectuer : set address "Ethernet 2" static 192.168.1.11 255.255.255.0 192.168.1.1
cela vas lui demandais de changer l adresse de la carte ethernet utiliser grace a son nom ici Ethernet 2 on peut le verifier avec un ipconfig/all et trouver la carte (Ethernet "NOM DE TA CARTE") 

## Prouvez que la connexion est fonctionnelle entre les deux machines

il suffit juste de faire un ping suivi de l adresse IP 

## Wireshark it
- lancer wireshark 
- cliquer sur votre carte Ethernet ici "Ethernet 2"
- effectuer un ping et regarder tout les ping rentrant en ICMP
le ping recu est de type 8 (echo)

# ARP 
## Check the ARP table
- arp -a puis nous pouvons y voir son IP suivi de son MAC ici "08-8f-c3-4b-cc-81"
- ipconfig /all pour retrouver l ip de la passeralle 
- arp -a "00-c0-e7-e0-04-4e"

# vider une table ARP 
- ARP -d vide la table ARP 
- ping 192.168.1.10
- arp -a plus d IP s affichera 

# wireshark it
- AsixElec_4c:e9:10 Broadcast premier est une requeste "PING"
- 08:8f:c3:4b:cc:81  AsixElec_4c:e9:10 le deuxieme est la reponse de l adresse que j ai ping 

## DHCP you too my brooo
- l adresse source du Discover et du Request sont les meme 0.0.0.0
- l adresse source du Offer et du ACK sont les meme    10.33.19.254

- leur destination sont touse les meme 255.255.255.255

- IP recu dans ACK ici 10.33.16.248
- IP de la passerelle 10.33.19.254
- les adresse DNS : 8.8.8.8
                  : 8.8.4.4
                  : 1.1.1.1