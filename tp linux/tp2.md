# Appréhender l environement linux 
## Service ssh 
------------------------------------------------------------------
### s'assurer que le service sshd est lancer
```bash
[enzo@tplinux2 ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-12-05 11:33:55 CET; 44min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 696 (sshd)
      Tasks: 1 (limit: 11119)
     Memory: 5.5M
        CPU: 138ms
     CGroup: /system.slice/sshd.service
             └─696 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"
```
------------------------------------------------------------------
###  Analyser les processus liés au service SSHù
```bash
[enzo@tplinux2 ~]$ ps |grep ps
   1474 pts/0    00:00:00 ps
```
------------------------------------------------------------------
### Déterminer le port sur lequel écoute le service SSH 
```bash
[enzo@tplinux2 ~]$ sudo ss  -ltunp |grep sshp
tcp   LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=696,fd=3))
tcp   LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=696,fd=4))
```
------------------------------------------------------------------
### Consulter les logs du service SSH
```bash
[enzo@tplinux2 log]$ sudo cat secure | grep ssh | tail -n 10
Dec  5 11:45:25 localhost sshd[1373]: pam_unix(sshd:session): session opened for user enzo(uid=1000) by (uid=0)
Dec  5 11:46:35 localhost sshd[1377]: Received disconnect from 10.4.1.1 port 57780:11: disconnected by user
Dec  5 11:46:35 localhost sshd[1377]: Disconnected from user enzo 10.4.1.1 port 57780
Dec  5 11:46:35 localhost sshd[1373]: pam_unix(sshd:session): session closed for user enzo
Dec  5 11:46:44 localhost sshd[1404]: Accepted password for enzo from 10.4.1.1 port 57786 ssh2
Dec  5 11:46:44 localhost sshd[1404]: pam_unix(sshd:session): session opened for user enzo(uid=1000) by (uid=0)
Dec  6 08:40:29 localhost sshd[1681]: Accepted password for enzo from 10.4.1.1 port 54524 ssh2
Dec  6 08:40:29 localhost sshd[1681]: pam_unix(sshd:session): session opened for user enzo(uid=1000) by (uid=0)
Dec  6 08:53:02 localhost sshd[1404]: pam_unix(sshd:session): session closed for user enzo
Dec  6 08:59:18 localhost sudo[1758]:    enzo : TTY=pts/1 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure -xe -u sshd
```
------------------------------------------------------------------
# Modification du service

###  Identifier le fichier de configuration du serveur SSH
```bash
[enzo@tplinux2 ~]$ cd /etc/ssh/
[enzo@tplinux2 ssh]$ ls -al
-rw-------.  1 root root       3669 Sep 20 20:46 sshd_config
```
------------------------------------------------------------------
### Modifier le fichier de conf
```bash
[enzo@tplinux2 ssh]$ sudo cat sshd_config |grep 1254
Port 1254

[enzo@tplinux2 ssh]$ sudo firewall-cmd --list-all |grep 1254
ports: 1254/tcp
```
------------------------------------------------------------------
### Redémarrer le service
```bash
[enzo@tplinux2 ~]$  sudo systemctl restart sshd
``` 
------------------------------------------------------------------
### Effectuer une connexion SSH sur le nouveau port
```bash
PS C:\Users\enzon> ssh enzo@10.4.1.2 -p 1254
```
------------------------------------------------------------------
# Service HTTP

### Installer le serveur NGINX
```bash
sudo dnf install nginx
```

### demarrer le serveur 
```bash
sudo systemctl enable nginx   

sudo firewall-cmd --permanent --add-service=http

sudo firewall-cmd --reload
```
------------------------------------------------------------------
### Déterminer sur quel port tourne NGINX
```bash
[enzo@tplinux2 ~]$ sudo ss -alnpt | grep nginx    
- LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=11972,fd=6),("nginx",pid=11971,fd=6))
- LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=11972,fd=7),("nginx",pid=11971,fd=7))
``` 
------------------------------------------------------------------
### Déterminer les processus liés à l'exécution de NGINX
[enzo@tplinux2 ~]$ ps -ef |grep nginx
```bash
- root       11971       1  0 10:26 ?        00:00:00 nginx: master process /usr/sbin/nginx
- nginx      11972   11971  0 10:26 ?        00:00:00 nginx: worker process
- enzo       12047    2097  0 11:01 pts/0    00:00:00 grep --color=auto nginx
``` 
------------------------------------------------------------------
###  Euh wait
```bash
[enzo@tplinux2 ~]$ curl http://10.4.1.2:80 |head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0  1240k      0 --:--:-- --:--:-- --:--:-- 1488k
curl: (23) Failed writing body
``` 
------------------------------------------------------------------*
# Analyser la conf de NGINX

### Déterminer le path du fichier de configuration de NGINX
```bash
[enzo@tplinux2 ~]$ cd /etc/nginx/
[enzo@tplinux2 nginx]$ ls -al

-rw-r--r--.  1 root root 2334 Oct 31 16:37 nginx.conf
``` 
------------------------------------------------------------------
### Trouver dans le fichier de conf
```bash
[enzo@tplinux2 nginx]$ cat nginx.conf | grep server -A 13*
 server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

```bash
[enzo@tplinux2 nginx]$ cat nginx.conf

# Load modular configuration files from the /etc/nginx/conf.d directory.
# See http://nginx.org/en/docs/ngx_core_module.html#include
# for more information.
 include /etc/nginx/conf.d/*.conf;
  ```
  ----------------------------------------------------------------


