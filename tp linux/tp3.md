# TP 3 : We do a little scripting
 
### Script carte d'identité

```bash
[enzo@enzo idcard]$ cat /srv/idcard/idcard.sh

echo "Machine Name $(hostnamectl --static)"

echo "OS $(uname) and kernel version id $(cat /etc/redhat-release)"

echo "IP $(ip a |head -9 | tail -1 |tr -s ' ' | cut -d' ' -f3 | cut -d'/' -f1)"

echo "RAM :$(free -h |grep Mem|tr -s ' '|cut -d' ' -f4) memory available on $(free -h |grep Swap |tr -s ' '|cut -d' ' -f4) total memory"

echo "Disk :$(df -h | head -5 | tail -1 | tr -s ' ' | cut -d '/' -f4 |cut -d ' ' -f4) space left "

echo "Top 5 processes by RAM usage :"
echo "$(ps -e -o command,%mem --sort=%mem |tail -5) "

echo " Listening ports :"
echo "$(ss -alp4H |tr -s ' ')"

cat_pic=$(curl -s https://cataas.com/cat > kitten)
ext=$(file --extension kitten | cut -d' ' -f2 | cut -d'/' -f1)
if [[ ${ext} == "jpeg" ]]
then
  cat_ext="cat.${ext}"
elif [[ ${ext} == "png" ]]
then
  cat_ext="cat.${ext}"
else
  cat_ext="cat.gif"
fi
mv kitten ${cat_ext}
chmod +x ${cat_ext}

echo " "
echo "Here is your random cat : ./${cat_ext}"

```

```bash
[enzo@enzo idcard]$ sudo ./idcard.sh
[sudo] password for enzo:
Machine Name enzo
OS Linux and kernel version id Rocky Linux release 9.0 (Blue Onyx)
IP 10.4.1.2
RAM :1.4Gi memory available on 2.0Gi total memory
Disk :16G space left
Top 5 processes by RAM usage :
/usr/lib/systemd/systemd-ud  0.6
/usr/lib/systemd/systemd --  0.7
/usr/lib/systemd/systemd --  0.9
/usr/sbin/NetworkManager --  1.0
/usr/bin/python3 -s /usr/sb  2.1
 Listening ports :
udp UNCONN 0 0 127.0.0.1:323 0.0.0.0:* users:(("chronyd",pid=654,fd=5))
tcp LISTEN 0 511 0.0.0.0:http 0.0.0.0:* users:(("nginx",pid=821,fd=6),("nginx",pid=816,fd=6))
tcp LISTEN 0 128 0.0.0.0:ssh 0.0.0.0:* users:(("sshd",pid=716,fd=3))

Here is your random cat : ./cat.jpeg
```
----------------------------------------------
## II. Script youtube-dl
```bash
#!/bin/bash

Titre_video="$(youtube-dl -e $1)"
cd /srv/yt/downloads/
mkdir "${Titre_video}" > /dev/null
cd "${Titre_video}"
youtube-dl $1 > /dev/null
echo "Vidéo" $1 "was downloaded."
nom_vid="$(ls *.mp4)"
youtube-dl --get-description $1 > description
echo "File path : /srv/yt/downloads/""${Titre_video}""/""${nom_vid}"
cd /var/log/yt
echo "[""$(date "+%Y/%m/%d"" ""%T")""]" >> /var/log/yt/download.log
echo "Vidéo" $1 "was downloaded. File path : /srv/yt/downloads/""${Titre_video}""/""${nom_vid}" >> /var/log/yt/download.log
```