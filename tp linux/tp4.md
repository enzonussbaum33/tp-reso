# Partie 1 : Partitionnement du serveur de stockage

# Partie 1 : Partitionnement du serveur de stockage

## 🌞 Partitionner le disque à l'aide de LVM
---
### - créer un physical volume (PV) :

```
[$enzo@localhost ~]$ sudo pvcreate /dev/sdb
[sudo] password for $enzo:
  Physical volume "/dev/sdb" successfully created.
```
----------------------------------------------------------------
### Je vérifie :

```
[enzo@localhost ~]$ sudo pvdisplay
  "/dev/sdb" is a new physical volume of "2.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               2.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               3kOEj7-m2Af-kSTp-ghAf-O8p8-bfq2-M7khQg
```
----------------------------------------------------------------
### - créer un nouveau volume group (VG)

```
[$enzo@localhost ~]$ sudo vgcreate localhost /dev/sdb
  Volume group "localhost" successfully created
```
--------------------------------------------------------------------------------
### Je vérifie :

```
[$enzo@localhost ~]$ sudo vgs
  VG      #PV #LV #SN Attr   VSize  VFree
  localhost   1   0   0 wz--n- <2.00g <2.00g
```
----------------------------------------------------------------
### - créer un nouveau logical volume (LV)

```
[$enzo@localhost ~]$ sudo lvcreate -l 100%FREE localhost -n data
  Logical volume "data" created.
```
----------------------------------------------------------------
### Je vérifie :

```
[$enzo@localhost ~]$ sudo lvs
  LV   VG      Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data localhost -wi-a----- <2.00g

```
----------------------------------------------------------------
## 🌞 Formater la partition

###  vous formaterez la partition en ext4 (avec une commande mkfs)

```
 [enzo@localhost ~]$ mkfs -t ext4 /dev/localhost/data
mke2fs 1.46.5 (30-Dec-2021)
mkfs.ext4: Permission denied while trying to determine filesystem size
[enzo@localhost ~]$ sudo mkfs -t ext4 /dev/localhost/data
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: 0947d64b-0711-4f73-b782-cf6f3639d3b2
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

```
----------------------------------------------------------------
## 🌞 Monter la partition

###  montage de la partition (avec la commande mount)

```
[$enzo@localhost ~]$ sudo mkdir /mnt/data1
[sudo] password for $enzo:

[$enzo@localhost ~]$ sudo mount /dev/localhost/data /mnt/data1/
```
----------------------------------------------------------------
### utilisez un | grep pour isoler les lignes intéressantes

```
[$enzo@localhost ~]$ df -h | grep data
/dev/mapper/localhost-data  2.0G   24K  1.9G   1% /mnt/data1
```
----------------------------------------------------------------
### Prouvez que vous pouvez lire et écrire des données sur cette partition

```
[$enzo@localhost ~]$ sudo touch /mnt/data1/lost+found/

[$enzo@localhost ~]$ ls
teste
```
----------------------------------------------------------------
### montage auto

```
[$enzo@localhost ~]$ cat /etc/fstab | grep localhost
/dev/mapper/localhost-data /mnt/data1 ext4 defaults 0 0

```
-----------------------------   
### vérifierez que le fichier /etc/fstab fonctionne 

```
[$enzo@localhost ~]$ sudo umount /mnt/data1
[$enzo@localhost ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/data1 does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/data1               : successfully mounted

```
--------------------------------------------------------------

# Partie 2:SERVEUR NFS 
```bash
On the Host and the Client 
[enzo@storage ~]$ sudo dnf install nfs-utils
[enzo@storage ~]$ cd /
```
```bash
on the host 
[enzo@storage ~]$ sudo mkdir /storage/site_web_1
[enzo@storage ~]$ sudo mkdir /storage/site_web_2

[enzo@storage storage]$ ls -l /storage/
drwxr-xr-x. 2 nobody root 38 Jan  2 16:03 site_web_1
drwxr-xr-x. 2 nobody root  6 Jan  2 16:06 site_web_2

[enzo@storage ~]$ sudo chown nobody /storage/site_web_1
[enzo@storage ~]$ sudo chown nobody /storage/site_web_2

[enzo@storage ~]$ sudo nano /etc/exports
/storage/site_web_1 10.4.1.4(rw,sync,no_subtree_check)
/storage/site_web_2 10.4.1.4(rw,sync,no_subtree_check)


[enzo@storage ~]$ sudo systemctl enable nfs-server
[enzo@storage ~]$ sudo systemctl start nfs-server

[enzo@storage ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Mon 2023-01-02 15:16:20 CET; 3h 45min ago
    Process: 881 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
    Process: 885 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 964 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=exi>
   Main PID: 964 (code=exited, status=0/SUCCESS)
        CPU: 46ms

[enzo@storage ~]$ firewall-cmd --permanent --list-all | grep services
services: cockpit dhcpv6-client ssh
[enzo@storage ~]$ firewall-cmd --permanent --add-service={nfs,mountd,rpc-bind}
[enzo@storage ~]$ firewall-cmd --reload
[enzo@storage ~]$ firewall-cmd --permanent --list-all | grep services
services: cockpit dhcpv6-client mountd nfs rpc-bind ssh
```
```bash
on the client
[enzo@web ~]$ sudo mkdir /var/www/site_web_1
[enzo@web ~]$ sudo mkdir /var/www/site_web_2
[enzo@web ~]$  sudo mount 10.4.1.3:/storage/site_web_1 /var/www/site_web_1
[enzo@web ~]$  sudo mount 10.4.1.3:/storage/site_web_2 /var/www/site_web_2

[enzo@web ~]$ sudo touch //var/www/site_web_1 test 
[enzo@web ~]$ ls -l /var/www/site_web_1
-rw-r--r--. 1 root root 0 Jan  2 19:47 test

[enzo@web ~]$ sudo touch //var/www/site_web_2 test 
[enzo@web ~]$ ls -l /var/www/site_web_1
-rw-r--r--. 1 root root 0 Jan  2 19:51 test
```
``` bash 
on client 

[enzo@web ~]$ sudo nano /etc/fstab
10.4.1.3:/var/www/site_web_1   /storage/site_web_1  nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
10.4.1.3:/var/www/site_web_2   /storage/site_web_2  nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

[enzo@web ~]$ cd ~ 
[enzo@web ~]$ sudo unmount /var/www/site_web_1 
[enzo@web ~]$ sudo unmount /var/www/site_web_2*

[enzo@web ~]$ sudo df -h
[sudo] password for enzo:
Filesystem                    Size  Used Avail Use% Mounted on
devtmpfs                      869M     0  869M   0% /dev
tmpfs                         888M     0  888M   0% /dev/shm
tmpfs                         356M  5.0M  351M   2% /run
/dev/mapper/rl-root            17G  1.2G   16G   7% /
/dev/sda1                    1014M  272M  743M  27% /boot
tmpfs                         178M     0  178M   0% /run/user/1000
```
------------------------------------------
# Partie 3 NGINX    

```ruby
[enzo@web ~]$ sudo dnf install nginx

[enzo@web ~]$ sudo systemctl start nginx
[enzo@web ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-02 21:21:34 CET; 14s ago
    Process: 1419 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1420 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1421 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1422 (nginx)
      Tasks: 2 (limit: 11119)
     Memory: 1.9M
        CPU: 22ms
     CGroup: /system.slice/nginx.service
             ├─1422 "nginx: master process /usr/sbin/nginx"
             └─1423 "nginx: worker process"
[enzo@web ~]$ sudo systemctl enable nginx
[sudo] password for enzo:
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

[enzo@web ~]$ sudo firewall-cmd --zone=public --permanent --add-service=http
[enzo@web ~]$ sudo firewall-cmd --reload

```

```ruby
[enzo@web ~]$ ps |grep ps
   1431 pts/0    00:00:00 ps
[enzo@web ~]$ ss |grep tcp
tcp     ESTAB    0  0   10.4.1.6:ssh  10.4.1.1:51525
```
---------------------------------
```ruby
[enzo@web ~]$ sudo nano /etc/host
10.4.1.6 myserver.com

[enzo@web ~]$ curl -H 'Host: myserver.com' localhost:80/hello
curl http://127.0.0.1/hello
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
etc...
</html>
```
--------------------------------
```ruby
