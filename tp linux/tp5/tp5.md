# tp5 

## installation

### installer le serveur Apache 

```ruby
[enzo@web ~]$ sudo dnf upgrade --refresh
[enzo@web ~]$ sudo dnf install httpd -y
[enzo@web ~]$ sudo systemctl enable httpd --now
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[enzo@web ~]$ sudo firewall-cmd --permanent --zone=public --add-service={http,https}
[enzo@web ~]$ sudo firewall-cmd --reload

[enzo@web ~]$ sudo systemctl enable httpd

[enzo@web ~]$ ss -altnp
State             Recv-Q      Send-Q    Local Address:Port    Peer Address:Port       Process
LISTEN            0           128         0.0.0.0:22            0.0.0.0:*
LISTEN            0           511         *:80                  *:*
LISTEN            0           128         [::]:22               [::]:*
```
--------------------------------------------------------------------------------------------------------------------------
## TEST
```ruby
[enzo@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor>     Active: active (running) since Tue 2023-01-03 16:15:05 CET; 12s ago
       Docs: man:httpd.service(8)
   Main PID: 47955 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; B>      Tasks: 213 (limit: 11119)
     Memory: 22.9M
        CPU: 75ms
     CGroup: /system.slice/httpd.service
             ├─47955 /usr/sbin/httpd -DFOREGROUND
             ├─47956 /usr/sbin/httpd -DFOREGROUND
             ├─47957 /usr/sbin/httpd -DFOREGROUND
             ├─47958 /usr/sbin/httpd -DFOREGROUND
             └─47959 /usr/sbin/httpd -DFOREGROUND

[enzo@web ~]$ sudo systemctl status httpd |grep enable
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)

[enzo@web ~]$ curl localhost |head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
<!doctype html>    0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   413k      0 --:--:-- --:--:-- --:--:--  437k

[enzo@web ~]$ curl 10.105.1.11 |head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```
-------------------------------------------------------
## 2.Avancer vers la maitrise du service

```ruby
[enzo@web /]$ cat /usr/lib/systemd/system/httpd.service

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful

KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target

[enzo@web conf]$ cat httpd.conf |grep User
User apache

[enzo@web conf]$ ps -ef |grep apache
apache     47956   47955  0 16:15 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     47957   47955  0 16:15 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     47958   47955  0 16:15 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache     47959   47955  0 16:15 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
enzo       48316    1303  0 17:40 pts/0    00:00:00 grep --color=auto apache

[enzo@web testpage]$ ls -al index.html
-rw-r--r--. 1 root root 7620 Jul 27 20:05 index.html

```
-------------------------------------------------------------------

## changer l utilisateur de apache
```ruby
[enzo@web ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
tss:x:59:59:Account used for TPM access:/dev/null:/sbin/nologin
sssd:x:998:995:User for sssd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/usr/share/empty.sshd:/sbin/nologin
chrony:x:997:994::/var/lib/chrony:/sbin/nologin
systemd-oom:x:992:992:systemd Userspace OOM Killer:/:/usr/sbin/nologin
enzo:x:1000:1000:enzo:/home/enzo:/bin/bash
tcpdump:x:72:72::/:/sbin/nologin
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin

[enzo@web ~]$ sudo useradd servweb -d /usr/share/httpd -u 2000 -s /sbin/nologin
servweb:x:2000:2000::/usr/share/httpd:/sbin/nologin

[enzo@web ~]$ sudo nano /etc/httpd/conf/httpd.conf

[enzo@web ~]$  cat /etc/httpd/conf/httpd.conf |grep User
User servweb

[enzo@web ~]$ sudo systemctl restart httpd

[enzo@web ~]$ ps -ef |grep httpd
servweb    48440   48439  0 18:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    48441   48439  2 18:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    48442   48439  1 18:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
servweb    48443   48439  1 18:04 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

[enzo@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
Warning: NOT_ENABLED: 80:tcp
success
[enzo@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[enzo@web ~]$ sudo firewall-cmd --reload
successListen 8080

[enzo@web ~]$ sudo systemctl restart httpd

[enzo@web ~]$ ss -altnp
State         Recv-Q   Send-Q   Local Address:Port   Peer Address:Port       Process
LISTEN        0        128       0.0.0.0:22           0.0.0.0:*
LISTEN        0        511       *:8080               *:*
LISTEN        0        128       [::]:22              [::]:*

[enzo@web ~]$ curl localhost:8080 |head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
<!doctype html>    0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   572k      0 --:--:-- --:--:-- --:--:--  572k


[enzo@web ~]$ curl 10.105.1.11:8080 |head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
100  7620  100  7620    0     0   826k      0 --:--:-- --:--:-- --:--:--  826k
