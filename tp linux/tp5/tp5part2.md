# PARTIE 2 Mise en place et maîtrise du serveur de base de données

##  Install de MariaDB sur db.tp5.linux 
```ruby
[enzo@db ~]$ sudo dnf install mariadb-server
Rocky Linux 9 - BaseOS                                                                  7.9 kB/s | 3.6 kB     00:00
Rocky Linux 9 - BaseOS                                                                  990 kB/s | 1.7 MB     00:01
Rocky Linux 9 - AppStream                                                                10 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                                                               344 kB/s | 6.4 MB     00:18
Rocky Linux 9 - Extras                                                                  5.2 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   16 kB/s | 8.5 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                                 Architecture      Version                           Repository            Size
========================================================================================================================
Installing:
 mariadb-server                          x86_64            3:10.5.16-2.el9_0                 appstream            9.4 M
 ---------------------------------------------------------
[enzo@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
-------------------------------------------------------------
[enzo@db ~]$ sudo systemctl start mariadb
-------------------------------------------------------------
[enzo@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] Y
Enabled successfully!
Reloading privilege tables..
 ... Success!
 ```
    
 ```ruby
 [enzo@db ~]$ sudo systemctl status mariadb|grep enable
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
```
##  Port utilisé par MariaDB

```ruby 
[enzo@db ~]$ ss -altnp |grep 3306
LISTEN 0      80                 *:3306            *:*

[enzo@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent

```

## Processus liés à MariaDB
```ruby
[enzo@db ~]$ ps -ef |grep mariadb
mysql      13242       1  0 10:36 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
enzo       13454    1301  0 11:11 pts/0    00:00:00 grep --color=auto mariadb
```

## Préparation de la base pour NextCloud

```ruby
[enzo@db ~]$ sudo mysql -u root -p
[sudo] password for enzo:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 15
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.017 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.002 sec)

```
##  Exploration de la base de données

```ruby
[enzo@web ~]$ sudo mysql -u nextcloud -h 10.105.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 22
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```


