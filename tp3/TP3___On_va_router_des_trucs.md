 # TP3 : On va router des trucs
 
 ### Générer des requêtes ARP

- sur vm john
ping 10.3.1.12
64 bytes from 10.3.1.12 ttl=64 time=1.59 ms

- sur vm marcel
ping 10.3.1.11
64 bytes from 10.3.1.11 ttl=64 time=1.59 ms

table ARP 

- sur vm john
ip neigh show 
10.3.1.12 dev enp0s8 lladdr 08:00:27:4d:52:dd(MAC) STALE

- sur vm marcel
ip neigh show 
10.3.1.11 dev enp0s8 lladdr 08:00:27:31:c7:d2(MAC) STALE

- sur vm john
ip neigh show 10.3.1.1
10.3.1.12 dev enp0s8 lladdr 08:00:27:4d:52:dd(MAC) STALE

- sur vm marcel 
ip a
link/ether 08:00:27:4d:52:dd

### Analyse de trames
voir tp3arp 


### Mise en place du routage
clone la machine originelle pour avoir un routeur

mettre les carte sur la machine 

activer le routeur
$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public --permanent

$ sudo firewall-cmd --list-all
si yes ca marche

sur john
sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8

sur marcel 
sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8

ping < IP de la machine > 

### Analyse des échanges ARP
voir "ping john marcel.pcap" et "pingjohnmarcel.pcap"
ordre | type trame  | ip source |    MACsource    | IP destination | MAC destination
1     requetes ARP              08:00:27:cf:56:a6                  Broadcast 08:00:27:83:57:c4
2     reponse ARP               08:00:27:83:57:c4                  john 08:00:27:cf:56:a6
3     ping requetes   10.3.1.11                     10.3.2.12  
4     ping reponse    10.3.2.12                     10.3.1.11
5     requetes ARP              08:00:27:4b:23:81                  broadcast 08:00:27:82:39:a4
6     reponse ARP               08:00:27:82:39:a4                  marcel 08:00:27:4b:23:81


### Donnez un accès internet à vos machines

sudo ip route add default 10.3.1.254(john) 10.3.2.254(marcel)
ping 8.8.8.8

sudo nano /etc/resolve.conf
verifier 1.1.1.1 sinon ajouter 
ping google.com

### Analyse de trames
voir "8888"
ordre | type trame  | ip source |    MACsource    | IP destination | MAC destination
1      ICMP request   10.3.1.11  08:00:27:cf:56:a6  8.8.8.8          08:00:27:83:57:c4
2      ICMP reply     8.8.8.8    08:00:27:83:57:c4  10.3.1.11        08:00:27:cf:56:a6




